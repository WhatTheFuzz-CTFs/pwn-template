# Pwn Template

## Description

My personal template for starting CTF challenges. This is used in combination
with `pwninit` to automate a lot of the initial steps.

## Installation

After installing [pwninit][pwninit], you can make this your default template
as described [here][custom].

## Authors and acknowledgment

Support the developers of [pwninit][pwninit]!

## License

In line with the original author's wishes, this template is licesed under the
MIT license.

[pwninit]: https://github.com/io12/pwninit
[custom]: https://github.com/io12/pwninit#custom-solvepy-template
